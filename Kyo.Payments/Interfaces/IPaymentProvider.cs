﻿using Kyo.Business.Objects;
using System.Threading.Tasks;

namespace Kyo.Payments.Interfaces
{
	public interface IPaymentProvider
	{
		/// <summary>
		/// Initiate a payment.
		/// </summary>
		/// <param name="order"> Order related to the payment.</param>
		/// <param name="locale"> ISO 15897 locale to use for the payment.</param>
		/// <param name="redirectUrl"> URL to redirect the customer to after completing the payment.</param>
		/// <returns></returns>
		Task<bool> Payment(Order order, string locale = "nl_NL", string redirectUrl = "/completed");
	}
}
