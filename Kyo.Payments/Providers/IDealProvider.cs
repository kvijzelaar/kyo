﻿using System;
using log4net;
using Mollie.Api.Client;
using Kyo.Business.Objects;
using System.Configuration;
using System.Threading.Tasks;
using Kyo.Payments.Interfaces;
using Mollie.Api.Models.Payment;
using Mollie.Api.Models.Payment.Request;

namespace Kyo.Payments.Providers
{
	public class IDealProvider : IPaymentProvider
	{
		#region Properties

		/// <summary>
		/// Log4Net instance.
		/// </summary>
		public ILog Log { get; set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="log"> Log4Net instance.</param>
		public IDealProvider(ILog log)
		{
			Log = log;
		}

		#endregion

		#region IPaymentProvider

		/// <summary>
		/// Initiate a payment.
		/// </summary>
		/// <param name="order"> Order related to the payment.</param>
		/// <param name="locale"> ISO 15897 locale to use for the payment.</param>
		/// <param name="redirectUrl"> URL to redirect the customer to after completing the payment.</param>
		/// <returns></returns>
		public async Task<bool> Payment(Order order, string locale = "nl_NL", string redirectUrl = "/completed")
		{
			try
			{
				// Initialize a new payment client.
				var client = new PaymentClient(ConfigurationManager.AppSettings["Mollie:Key"]);

				// Initiate a payment.
				var result = await client.CreatePaymentAsync(new PaymentRequest()
				{
					Locale = locale,
					Amount = order.Amount,
					RedirectUrl = redirectUrl,
					Method = PaymentMethod.Ideal,
					Description = $"Bestelling {order.Id}"
				});

				return result.Status.HasValue && result.Status == PaymentStatus.Paid;
			}
			catch(Exception ex)
			{
				Log.Error(ex.Message, ex);

				return false;
			}
		}

		#endregion
	}
}
