﻿using System;
using Kyo.Database.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kyo.Database.Entities
{
    [Table("Products", Schema = "dbo")]
    public class Product : IEntity<int>, ICreatedOn, IUpdatedOn
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        public bool Enabled { get; set; }

		#region ICreatedOn

		public DateTime CreatedOn { get; set; }

		#endregion

		#region IUpdatedOn

		public DateTime? UpdatedOn { get; set; }

		#endregion
	}
}
