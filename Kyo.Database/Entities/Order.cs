﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kyo.Database.Entities
{
    [Table("Orders", Schema = "dbo")]
    public class Order
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime? PlacedOn { get; set; }

        public DateTime? PayedOn { get; set; }

        public DateTime? ShippedOn { get; set; }

        public string Description { get; set; }

        [Required]
        public byte PaymentMethod { get; set; }

        public bool IsConfirmed { get; set; }

        public virtual ICollection<OrderItem> Items { get; set; }
    }
}
