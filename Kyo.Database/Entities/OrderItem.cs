﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kyo.Database.Entities
{
    [Table("OrderItems", Schema = "dbo")]
    public class OrderItem
    {
        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public decimal TaxPercentage { get; set; }

        public decimal? Discount { get; set; }
    }
}
