﻿using System;
using log4net;
using System.Linq;
using System.Data.Entity;
using Kyo.Database.Interfaces;
using System.Linq.Expressions;
using Kyo.Database.Extensions;
using System.Collections.Generic;

namespace Kyo.Database.Repositories
{
    /// <summary>
    /// Default entity repository implementation.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public class Repository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : class, IEntity<TId> where TId : IComparable
    {
        #region Properties

        /// <summary>
        /// Default Log4Net instance.
        /// </summary>
        public ILog Log { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="log"> Log4Net instance.</param>
        public Repository(ILog log)
        {
            Log = log;
        }

        #endregion

        #region IRepository

        /// <summary>
        /// Retrieves all the entities.
        /// </summary>
        /// <returns>IEnumerable of entities or null.</returns>
        public IEnumerable<TEntity> All(params Expression<Func<TEntity, object>>[] includes)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    return context.Set<TEntity>().IncludeMultiple(includes).AsNoTracking();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Retrieves all the entities matching the predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>IEnumerable of entities or null.</returns>
        public IEnumerable<TEntity> All(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    return context.Set<TEntity>().AsNoTracking().IncludeMultiple(includes).Where(predicate);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        public TEntity FirstOrDefault(TId id, params Expression<Func<TEntity, object>>[] includes)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    return context.Set<TEntity>().AsNoTracking().IncludeMultiple(includes).FirstOrDefault(e => e.Id.CompareTo(id) == 0);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    return context.Set<TEntity>().AsNoTracking().IncludeMultiple(includes).FirstOrDefault(predicate);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Create the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Entity with the Id specified if the action succeeded.</returns>
        public TEntity Create(TEntity entity)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    // Add the entity to the DbSet.
                    entity = context.Set<TEntity>().Add(entity);

                    if (context.SaveChanges() > 0)
                    {
                        return entity;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Update the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated entity or null if the action failed.</returns>
        public TEntity Update(TEntity entity)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    // Attach the entity to the DbSet.
                    context.Set<TEntity>().Attach(entity);

                    // Specify the entity as modified.
                    context.Entry<TEntity>(entity).State = EntityState.Modified;

                    if (context.SaveChanges() > 0)
                        return entity;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return null;
        }

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>True or False if the action failed.</returns>
        public bool Delete(TEntity entity)
        {
            try
            {
                using (var context = new ApplicationContext(Log))
                {
                    // Delete the entity.
                    context.Set<TEntity>().Remove(entity);

                    return context.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }

            return false;
        }

        #endregion
    }
}
