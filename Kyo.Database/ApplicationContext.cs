﻿using System;
using log4net;
using System.Linq;
using System.Data.Entity;
using Kyo.Database.Entities;
using Kyo.Database.Interfaces;

namespace Kyo.Database
{
    /// <summary>
    /// Default DbContext used in the application.
    /// </summary>
    public class ApplicationContext : DbContext
    {
        #region Properties

        /// <summary>
        /// Default Log4Net instance.
        /// </summary>
        public ILog Log { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor, disables the database initializer.
        /// </summary>
        public ApplicationContext(ILog log) : base("DefaultConnection")
        {
            Log = log;

            // Disable the database initializer.
            System.Data.Entity.Database.SetInitializer<ApplicationContext>(null);

            // Disable lazy loading.
            this.Configuration.LazyLoadingEnabled = false;
        }

        #endregion

        #region Database Sets

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public DbSet<Product> Products { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// OnModelCreating enables the database entity to specify relationships.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Execute the related Entity OnModelCreating methods.
            //User.OnModelCreating(modelBuilder);
            //Site.OnModelCreating(modelBuilder);
            //Entities.Configuration.OnModelCreating(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

		/// <summary>
		/// Custom implementation of the SaveChanges method that updates the required properties specified by interfaces.
		/// </summary>
		/// <returns></returns>
		public override int SaveChanges()
		{
			try
			{
				// Detect any changes made to the tracked entities.
				ChangeTracker.DetectChanges();

				// Retrieve all the recently added or modified entities.
				var createdEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToArray();
				var updatedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToArray();

				// Retrieve the entity count.
				var createdEntityCount = createdEntities.Count();
				var updatedEntityCount = updatedEntities.Count();

				// Process the created entities.
				for (var i = 0; i < createdEntityCount; i++)
				{
					if (createdEntities[i].Entity is ICreatedOn)
					{
						var entity = createdEntities[i].Entity as ICreatedOn;

						if (entity != null)
						{
							// Update the CreatedOn value with the UTC datetime.
							if (entity.CreatedOn == DateTime.MinValue)
								entity.CreatedOn = DateTime.UtcNow;
						}
					}
				}

				// Process the updated entities.
				for (var i = 0; i < updatedEntityCount; i++)
				{
					if (updatedEntities[i].Entity is IUpdatedOn)
					{
						var entity = updatedEntities[i].Entity as IUpdatedOn;

						if (entity != null)
						{
							// Update the UpdatedOn value with the UTC datetime.
							entity.UpdatedOn = DateTime.UtcNow;
						}
					}
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex.Message, ex);
			}

			return base.SaveChanges();
		}

		#endregion
	}
}
