﻿using System;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Kyo.Database.Extensions
{
    /// <summary>
    /// Extension methods on the IQueryable object.
    /// </summary>
    public static class IQueryableExtensions
    {
        /// <summary>
        /// Enables support for multiple include specifications.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        public static IQueryable<T> IncludeMultiple<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] includes) where T : class
        {
            // Verify that includes are specified.
            if (includes != null && includes.Length > 0)
            {
                // Loop each include specification.
                query = includes.Aggregate(query, (current, include) => current.Include(include));
            }

            return query;
        }
    }
}
