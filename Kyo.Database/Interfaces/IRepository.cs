﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Kyo.Database.Interfaces
{
    /// <summary>
    /// Minimal specification for the repositories used in the Database project.
    /// </summary>
    /// <typeparam name="TEntity">Entity implementing the IEntity interface.</typeparam>
    /// <typeparam name="TId">Identifier for the entity.</typeparam>
    public interface IRepository<TEntity, TId> where TId : IComparable
    {
        /// <summary>
        /// Retrieves all the entities.
        /// </summary>
        /// <returns>IEnumerable of entities.</returns>
        IEnumerable<TEntity> All(params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Retrieves all the entities matching the predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>IEnumerable of entities.</returns>
        IEnumerable<TEntity> All(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        TEntity FirstOrDefault(TId id, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        /// <summary>
        /// Create the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Entity with the Id specified if the action succeeded.</returns>
        TEntity Create(TEntity entity);

        /// <summary>
        /// Update the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated entity or null if the action failed.</returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>True or False if the action failed.</returns>
        bool Delete(TEntity entity);
    }
}
