﻿namespace Kyo.Database.Interfaces
{
    /// <summary>
    /// Interface specifying the required properties for entities used in the database repositories.
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public interface IEntity<TId>
    {
        /// <summary>
        /// Entity identifier with a generic Type specifier.
        /// </summary>
        TId Id { get; set; }
    }
}
