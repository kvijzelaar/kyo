﻿using System;

namespace Kyo.Database.Interfaces
{
	public interface IUpdatedOn
	{
		DateTime? UpdatedOn { get; set; }
	}
}
