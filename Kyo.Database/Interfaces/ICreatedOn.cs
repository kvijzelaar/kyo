﻿using System;

namespace Kyo.Database.Interfaces
{
	public interface ICreatedOn
	{
		DateTime CreatedOn { get; set; }
	}
}
