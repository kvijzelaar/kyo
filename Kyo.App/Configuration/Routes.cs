﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Kyo.App.Configuration
{
	public static class Routes
	{
		public static void Register(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapMvcAttributeRoutes();
		}
	}
}
