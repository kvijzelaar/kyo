﻿using Autofac;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Kyo.Shared.DependencyInjection;

namespace Kyo.App.Configuration
{
    public static class DependencyInjection
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();

            // Register the MVC controllers.
            builder.RegisterControllers(typeof(Application).Assembly);

            // Register the specified dependencies.
            Bootstrapper.Register(builder);

            // Build a dependency container.
            var dependencyContainer = builder.Build();

            // Configure the default MVC dependency resolver.
            DependencyResolver.SetResolver(new AutofacDependencyResolver(dependencyContainer));
        }
    }
}