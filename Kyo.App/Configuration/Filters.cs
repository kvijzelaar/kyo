﻿using System.Web.Mvc;

namespace Kyo.App.Configuration
{
	public static class Filters
	{
		public static void Register(GlobalFilterCollection filters)
		{
			filters.Add(new AuthorizeAttribute());
			filters.Add(new HandleErrorAttribute());
		}
	}
}
