﻿using System;
using Kyo.Business.Objects;
using Microsoft.AspNet.Identity;

namespace Kyo.App.Authentication
{
	public class ApplicationUserManager : UserManager<ApplicationUser>
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="store"> User store instance.</param>
		public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store)
		{
			// Configure the default user validation policy.
			UserValidator = new UserValidator<ApplicationUser>(this)
			{
				RequireUniqueEmail = true,
				AllowOnlyAlphanumericUserNames = false
			};

			// Configure the default password validation policy.
			PasswordValidator = new PasswordValidator
			{
				RequiredLength = 10,
				RequireDigit = true,
				RequireLowercase = true,
				RequireUppercase = true,
				RequireNonLetterOrDigit = true
			};

			// Configure the default lockout policy.
			UserLockoutEnabledByDefault = true;
			MaxFailedAccessAttemptsBeforeLockout = 5;
			DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(15);
		}

		#endregion
	}
}