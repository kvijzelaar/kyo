﻿using Kyo.Business.Objects;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Kyo.App.Authentication
{
	// Configure the application sign-in manager which is used in this application.
	public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="userManager"> User manager.</param>
		/// <param name="authenticationManager"> Authentication manager.</param>
		public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager) { }

		#endregion

		/// <summary>
		/// Generate a ClaimsIdentity object for the user.
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
		{
			return UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
		}
	}
}