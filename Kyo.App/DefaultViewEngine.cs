﻿using System.Web.Mvc;

namespace Kyo.App
{
    /// <summary>
    /// Optimized implementation of the RazorViewEngine.
    /// </summary>
    public class DefaultViewEngine : RazorViewEngine
    {
        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public DefaultViewEngine() : base()
        {
            // Override the default View locations.
            OverrideViewLocations();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="viewPageActivator">View page activator instance</param>
        public DefaultViewEngine(IViewPageActivator viewPageActivator) : base(viewPageActivator)
        {
            // Override the default View locations.
            OverrideViewLocations();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Overrides the default View locations for improved performance.
        /// </summary>
        private void OverrideViewLocations()
        {
            AreaViewLocationFormats = new[]
            {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml"
            };

            AreaMasterLocationFormats = new[]
            {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml"
            };

            AreaPartialViewLocationFormats = new[]
            {
                "~/Areas/{2}/Views/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/Shared/{0}.cshtml"
            };

            ViewLocationFormats = new[]
            {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            MasterLocationFormats = new[]
            {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            PartialViewLocationFormats = new[]
            {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml"
            };

            FileExtensions = new[]
            {
                "cshtml"
            };
        }

        #endregion
    }
}