﻿using Autofac;
using Autofac.log4net;
using System.Reflection;
using Kyo.Business.Objects;
using Kyo.App.Authentication;
using Microsoft.AspNet.Identity.Owin;
using Kyo.Shared.DependencyInjection.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Kyo.App
{
	/// <summary>
	/// Register the AutoMapper dependencies.
	/// </summary>
	public class Dependency : IDependency
	{
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		public Dependency() { }

		#endregion

		#region IDependency

		/// <summary>
		/// Registers dependencies that should be resolved by AutoFac.
		/// </summary>
		/// <param name="builder">DI container builder</param>
		/// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
		/// <returns>DI container builder</returns>
		public ContainerBuilder Register(ContainerBuilder builder, Assembly[] referencedAssemblies)
		{
			// Register the Log4Net module.
			builder.RegisterModule(new Log4NetModule()
			{
				ConfigFileName = "log4net.config",
				ShouldWatchConfiguration = true
			});

			// Register the required Identity objects.
			builder.Register(c => new ApplicationDbContext()).AsSelf().InstancePerLifetimeScope();
			builder.Register(c => new UserStore<ApplicationUser>()).AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.Register(c => new IdentityFactoryOptions<ApplicationUserManager>()
			{
				DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionPr‌​ovider("Kyo")
			});

			builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerLifetimeScope();

			return builder;
		}

		#endregion
	}
}