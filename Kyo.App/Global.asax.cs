﻿using System.Web.Mvc;
using System.Web.Routing;
using Kyo.App.Configuration;
using System.Web.Optimization;

namespace Kyo.App
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			// Clear any pre-existing View Engine and only add the RazorViewEngine.
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new DefaultViewEngine());

			// Register dependencies.
			DependencyInjection.Register();

			// Register application HTTP routes.
			Routes.Register(RouteTable.Routes);

			// Register application filters.
			Filters.Register(GlobalFilters.Filters);

			// Register the resource bundles.
			Bundles.Register(BundleTable.Bundles);
		}
	}
}
