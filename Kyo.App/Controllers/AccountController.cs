﻿using log4net;
using System.Web.Mvc;
using System.Threading.Tasks;
using Kyo.App.Authentication;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Kyo.App.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
		#region Properties

		/// <summary>
		/// User manager.
		/// </summary>
		public ApplicationUserManager UserManager { get; set; }

		/// <summary>
		/// User sign-in manager.
		/// </summary>
		public ApplicationSignInManager SignInManager { get; set; }

		/// <summary>
		/// Authentication manager.
		/// </summary>
		public IAuthenticationManager AuthenticationManager { get; set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="log"> Log4Net instance.</param>
		/// <param name="userManager"> User manager.</param>
		/// <param name="signInManager"> User sign-in manager.</param>
		/// <param name="authenticationManager"> Authentication manager.</param>
		public AccountController(ILog log, ApplicationUserManager userManager, ApplicationSignInManager signInManager, IAuthenticationManager authenticationManager) : base(log)
		{
			UserManager = userManager;
			SignInManager = signInManager;
			AuthenticationManager = authenticationManager;
		}

		#endregion

		#region Sign In

		[HttpGet]
        [AllowAnonymous]
		[Route("~/login", Name = "Account.Login", Order = 0)]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
		[Route("~/login", Name = "Account.Authenticate", Order = 1)]
        public async Task<ActionResult> Authenticate(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
					return RedirectToRoute("Pages.Index");
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

		#endregion

		#region Register

		[HttpGet]
        [AllowAnonymous]
		[Route("~/register", Name = "Account.Registration", Order = 2)]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
		[Route("~/register", Name = "Account.Register", Order = 3)]
		public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };

                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

					return RedirectToRoute("Pages.Index");
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

		#endregion

        [HttpGet]
		[Route("logout", Name = "Account.Logout", Order = 4)]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

			return RedirectToRoute("Pages.Index");
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        #endregion
    }
}