﻿using log4net;
using System.Web.Mvc;

namespace Kyo.App.Controllers
{
    public class BaseController : Controller
	{
		#region Properties

		/// <summary>
		/// Default Log4Net instance.
		/// </summary>
		protected ILog Log { get; set; }

		#endregion

		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="log"> Log4Net instance.</param>
		public BaseController(ILog log)
		{
			Log = log;
		}

		#endregion
	}
}