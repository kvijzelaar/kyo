﻿using log4net;
using System.Web.Mvc;

namespace Kyo.App.Controllers
{
    public class PagesController : BaseController
    {
		#region Constructor

		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="log"> Log4Net instance.</param>
		public PagesController(ILog log) : base(log) { }

		#endregion

		[HttpGet]
		[AllowAnonymous]
		[Route("", Name = "Pages.Index", Order = 0)]
		public ActionResult Index()
		{
			return View("~/Views/Pages/Index.cshtml");
		}

		[HttpGet]
		[AllowAnonymous]
		[Route("contact", Name = "Pages.Contact", Order = 1)]
		public ActionResult Contact()
		{
			return View("~/Views/Pages/Contact.cshtml");
		}
    }
}