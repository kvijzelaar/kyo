﻿using System;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Kyo.Business.Interfaces
{
    public interface IService<TObject, TEntity, TId>
    {
        /// <summary>
        /// Retrieves all the entities.
        /// </summary>
        /// <returns>IEnumerable of entities.</returns>
        IEnumerable<TObject> All(params Expression<Func<TObject, object>>[] includes);

        /// <summary>
        /// Retrieves all the entities matching the predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>IEnumerable of entities.</returns>
        IEnumerable<TObject> All(Expression<Func<TObject, bool>> predicate, params Expression<Func<TObject, object>>[] includes);

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        TObject FirstOrDefault(TId id, params Expression<Func<TObject, object>>[] includes);

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        TObject FirstOrDefault(Expression<Func<TObject, bool>> predicate, params Expression<Func<TObject, object>>[] includes);

        /// <summary>
        /// Create the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Entity with the Id specified if the action succeeded.</returns>
        TObject Create(TObject entity);

        /// <summary>
        /// Update the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated entity or null if the action failed.</returns>
        TObject Update(TObject entity);

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>True or False if the action failed.</returns>
        bool Delete(TObject entity);
    }
}
