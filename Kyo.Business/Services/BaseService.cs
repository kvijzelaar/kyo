﻿using System;
using log4net;
using System.Linq;
using System.Linq.Expressions;
using Kyo.Business.Interfaces;
using Kyo.Database.Interfaces;
using Kyo.Database.Repositories;
using System.Collections.Generic;
using Kyo.Shared.ObjectMapper.Interfaces;

namespace Kyo.Business.Services
{
    /// <summary>
    /// Base implementation of the IService interface.
    /// </summary>
    /// <typeparam name="TObject"></typeparam>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public abstract class BaseService<TObject, TEntity, TId> : IService<TObject, TEntity, TId> where TObject : class, new() where TEntity : class, IEntity<TId> where TId : IComparable
    {
        #region Properties

        /// <summary>
        /// Log4Net instance.
        /// </summary>
        public ILog Log { get; set; }

        /// <summary>
        /// AutoMapper wrapper instance.
        /// </summary>
        public ISharedMapper<TObject, TEntity> Mapper { get; set; }

        /// <summary>
        /// Database entities repository.
        /// </summary>
        public IRepository<TEntity, TId> Repository { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor, initializes the entity repository.
        /// </summary>
        /// <param name="mapper"> Default object mapper.</param>
        /// <param name="log"> Log4Net instance.</param>
        public BaseService(ISharedMapper<TObject, TEntity> mapper, ILog log)
        {
            Mapper = mapper;
            Repository = new Repository<TEntity, TId>(log);
        }

        #endregion

        #region IService

        /// <summary>
        /// Retrieves all the entities.
        /// </summary>
        /// <returns>IEnumerable of entities.</returns>
        public IEnumerable<TObject> All(params Expression<Func<TObject, object>>[] includes)
        {
            var entityIncludes = Mapper.Map<Expression<Func<TObject, object>>[], Expression<Func<TEntity, object>>[]>(includes);

            return Repository.All(entityIncludes).Select(e => Mapper.Map(e));
        }

        /// <summary>
        /// Retrieves all the entities matching the predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>IEnumerable of entities.</returns>
        public IEnumerable<TObject> All(Expression<Func<TObject, bool>> predicate, params Expression<Func<TObject, object>>[] includes)
        {
            var entityIncludes = Mapper.Map<Expression<Func<TObject, object>>[], Expression<Func<TEntity, object>>[]>(includes);

            return Repository.All(Mapper.Map<Expression<Func<TObject, bool>>, Expression<Func<TEntity, bool>>>(predicate), entityIncludes).Select(e => Mapper.Map(e));
        }

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        public TObject FirstOrDefault(TId id, params Expression<Func<TObject, object>>[] includes)
        {
            var entityIncludes = Mapper.Map<Expression<Func<TObject, object>>[], Expression<Func<TEntity, object>>[]>(includes);

            return Mapper.Map(Repository.FirstOrDefault(id, entityIncludes));
        }

        /// <summary>
        /// Retrieves the first matching entity or returns null.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>Entity or null.</returns>
        public TObject FirstOrDefault(Expression<Func<TObject, bool>> predicate, params Expression<Func<TObject, object>>[] includes)
        {
            var entityIncludes = Mapper.Map<Expression<Func<TObject, object>>[], Expression<Func<TEntity, object>>[]>(includes);

            return Mapper.Map(Repository.FirstOrDefault(Mapper.Map<Expression<Func<TObject, bool>>, Expression<Func<TEntity, bool>>>(predicate), entityIncludes));
        }

        /// <summary>
        /// Create the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Entity with the Id specified if the action succeeded.</returns>
        public TObject Create(TObject entity)
        {
            return Mapper.Map(Repository.Create(Mapper.Map(entity)));
        }

        /// <summary>
        /// Update the entity in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Updated entity or null if the action failed.</returns>
        public TObject Update(TObject entity)
        {
            return Mapper.Map(Repository.Update(Mapper.Map(entity)));
        }

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>True or False if the action failed.</returns>
        public bool Delete(TObject entity)
        {
            return Repository.Delete(Mapper.Map(entity));
        }

        #endregion
    }
}
