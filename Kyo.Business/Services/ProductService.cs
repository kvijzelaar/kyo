﻿using log4net;
using Kyo.Shared.ObjectMapper.Interfaces;

using DbEntities = Kyo.Database.Entities;
using BusinessObjects = Kyo.Business.Objects;

namespace Kyo.Business.Services
{
	public class ProductService : BaseService<BusinessObjects.Product, DbEntities.Product, int>
    {
        #region Constructor

        /// <summary>
        /// Default constructor, initializes the entity repository.
        /// </summary>
        public ProductService(ISharedMapper<BusinessObjects.Product, DbEntities.Product> mapper, ILog log) : base(mapper, log) { }

        #endregion
    }
}
