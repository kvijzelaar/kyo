﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kyo.Business.Objects
{
	public class Order
	{
		public long Id { get; set; }

		public decimal Amount { get; set; }
	}
}
