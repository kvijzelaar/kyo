﻿using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;

namespace Kyo.Business.Objects
{
    public class ApplicationUser : IUser
    {
		[Required]
		public string Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
