﻿using Autofac;
using System.Linq;
using System.Reflection;
using Kyo.Business.Interfaces;
using Kyo.Shared.DependencyInjection.Interfaces;

namespace Kyo.Business
{
    /// <summary>
    /// Register the AutoMapper dependencies.
    /// </summary>
    public class Dependency : IDependency
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Dependency() { }

        #endregion

        #region IDependency

        /// <summary>
        /// Registers dependencies that should be resolved by AutoFac.
        /// </summary>
        /// <param name="builder">DI container builder</param>
        /// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
        /// <returns>DI container builder</returns>
        public ContainerBuilder Register(ContainerBuilder builder, Assembly[] referencedAssemblies)
        {
            // Retrieve the current assembly.
            var assembly = typeof(Dependency).Assembly;

            if (assembly != null)
            {
                // Retrieve the IService type.
                var serviceType = typeof(IService<,,>).GetGenericTypeDefinition();

                builder.RegisterAssemblyTypes(assembly).Where(t => t.GetInterfaces()
                    .Where(e => e.IsGenericType)
                    .Select(e => e.GetGenericTypeDefinition())
                    .Any(e => e.Equals(serviceType))).InstancePerLifetimeScope();
            }

            return builder;
        }

        #endregion
    }
}
