﻿CREATE TABLE [dbo].[Orders]
(
	[Id]			BIGINT IDENTITY(1,1) NOT NULL,
	[CustomerId]	BIGINT NOT NULL,
	[PlacedOn]		DATETIME2(5) NULL,
	[PayedOn]		DATETIME2(5) NULL,
	[ShippedOn]		DATETIME2(5) NULL,
	[Description]	NVARCHAR(MAX) NULL,
	[PaymentMethod]	SMALLINT NOT NULL,
	[IsConfirmed]	BIT DEFAULT 0 NOT NULL,
	CONSTRAINT [PK_Orders] PRIMARY KEY ([Id])
)
