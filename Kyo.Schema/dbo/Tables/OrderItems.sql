﻿CREATE TABLE [OrderItems]
(
	[OrderId]		BIGINT NOT NULL,
	[ProductId]		INT NOT NULL,
	[Amount]		INT NOT NULL,
	[Price]			DECIMAL(10,2) NOT NULL,
	[TaxPercentage] DECIMAL(10,2) NOT NULL,
	[Discount]		DECIMAL(10,2) NULL,
	CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders],
	CONSTRAINT [FK_OrderItems_Products] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Products]
)
