﻿using System;
using Autofac;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Kyo.Shared.Services.Interfaces;
using Kyo.Shared.DependencyInjection.Interfaces;

namespace Kyo.Shared.Services
{
    /// <summary>
    /// Register the Service dependencies.
    /// </summary>
    public class Dependency : IDependency
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Dependency() { }

        #endregion

        #region IDependency

        /// <summary>
        /// Registers dependencies that should be resolved by AutoFac.
        /// </summary>
        /// <param name="builder">DI container builder</param>
		/// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
        /// <returns>DI container builder</returns>
        public ContainerBuilder Register(ContainerBuilder builder, Assembly[] referencedAssemblies)
        {
			// Retrieve the IService implementation types.
			var serviceTypes = GetAssemblyServiceTypes(referencedAssemblies);

            return builder;
        }

		#endregion

		#region Helpers

		/// <summary>
		/// Get all the IService implementation types in the referenced assemblies.
		/// </summary>
		/// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
		/// <returns></returns>
		private IEnumerable<Type> GetAssemblyServiceTypes(Assembly[] referencedAssemblies)
        {
            if (referencedAssemblies != null)
            {
                // Retrieve the Service type.
                var serviceType = typeof(IService);

				return referencedAssemblies.SelectMany(e => e.GetTypes().Where(t => !t.IsInterface && serviceType.IsAssignableFrom(t)));
            }

            return null;
        }

        #endregion
    }
}
