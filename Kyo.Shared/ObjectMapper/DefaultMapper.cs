﻿using AutoMapper;
using Kyo.Shared.ObjectMapper.Interfaces;

namespace Kyo.Shared.ObjectMapper
{
    /// <summary>
    /// Default AutoMapper wrapper implementation.
    /// </summary>
    /// <typeparam name="TSource">Source type</typeparam>
    /// <typeparam name="TDestination">Destination type</typeparam>
    public class DefaultMapper<TSource, TDestination> : ISharedMapper<TSource, TDestination> where TSource : class, new() where TDestination : class
    {
        #region Properties

        #region AutoMapper

        public IMapper Mapper { get; set; }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="mapper">AutoMapper IMapper instance.</param>
        public DefaultMapper(IMapper mapper)
        {
            Mapper = mapper;
        }

        #endregion

        #region ISharedMapper

        /// <summary>
        /// Map the source object to the destination object.
        /// </summary>
        /// <param name="source">TSource object</param>
        /// <returns>TDestination object</returns>
        public TDestination Map(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        /// <summary>
        /// Map the destination object to the source object.
        /// </summary>
        /// <param name="destination">TDestination object</param>
        /// <returns>TSource object</returns>
        public TSource Map(TDestination destination)
        {
            return Mapper.Map<TDestination, TSource>(destination);
        }

        /// <summary>
        /// Maps the source Expression to the destination Expression.
        /// </summary>
        /// <typeparam name="TSourceEx"></typeparam>
        /// <typeparam name="TDestinationEx"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public TDestinationEx Map<TSourceEx, TDestinationEx>(TSourceEx source)
        {
            //// Retrieve the Type of TSourceEx.
            //var sourceType = typeof(TSourceEx);

            //// Try to find a existing Type mapping.
            //var sourceTypeMapping = Mapper.ConfigurationProvider.FindTypeMapFor(sourceType, typeof(TDestinationEx));

            return Mapper.Map<TSourceEx, TDestinationEx>(source);
        }

        #endregion
    }
}
