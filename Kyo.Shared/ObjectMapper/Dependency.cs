﻿using System;
using Autofac;
using AutoMapper;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Kyo.Shared.ObjectMapper.Interfaces;
using Kyo.Shared.DependencyInjection.Interfaces;

namespace Kyo.Shared.ObjectMapper
{
    /// <summary>
    /// Register the AutoMapper dependencies.
    /// </summary>
    public class Dependency : IDependency
    {
        #region Constructor

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Dependency() { }

        #endregion

        #region IDependency

        /// <summary>
        /// Registers dependencies that should be resolved by AutoFac.
        /// </summary>
        /// <param name="builder">DI container builder</param>
		/// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
        /// <returns>DI container builder</returns>
        public ContainerBuilder Register(ContainerBuilder builder, Assembly[] referencedAssemblies)
        {
            var mappingConfiguration = new MapperConfiguration(cfg =>
            {
                // Get all the Profile implementation types.
                var profileTypes = GetAssemblyProfileTypes(referencedAssemblies);

                if (profileTypes != null)
                {
                    foreach (var profileType in profileTypes)
                    {
                        // Add a instance of the Profile type to the configuration.
                        cfg.AddProfile(Activator.CreateInstance(profileType) as Profile);
                    }
                }

                // Automatically create missing Type mappings.
                cfg.CreateMissingTypeMaps = true;
            });

            // Assert if the mapping configuration is valid.
            mappingConfiguration.AssertConfigurationIsValid();

            // Create and register the AutoMapper configuration.
            builder.Register(c => mappingConfiguration).AsSelf().SingleInstance();

            // Register the AutoMapper IMapper instance.
            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve)).As<IMapper>().InstancePerLifetimeScope();

            // Register the available IMapper implementations.
            builder.RegisterGeneric(typeof(DefaultMapper<,>)).As(typeof(ISharedMapper<,>));

            return builder;
        }

		#endregion

		#region Helpers

		/// <summary>
		/// Get all the AutoMapper Profile implementation types in the referenced assemblies.
		/// </summary>
		/// <param name="referencedAssemblies">Assemblies referenced by the BuildManager.</param>
		/// <returns></returns>
		private IEnumerable<Type> GetAssemblyProfileTypes(Assembly[] referencedAssemblies)
        {
            // Retrieve all the referenced assemblies.
            var assemblies = referencedAssemblies.Where(e => !e.FullName.Contains("AutoMapper")).ToArray();

            if (assemblies != null)
            {
                // Retrieve the Profile type.
                var profileType = typeof(Profile);

                return assemblies.SelectMany(e => e.GetTypes().Where(t => t.BaseType == profileType));
            }

            return null;
        }

        #endregion
    }
}
