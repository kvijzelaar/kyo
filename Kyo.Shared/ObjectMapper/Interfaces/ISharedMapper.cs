﻿namespace Kyo.Shared.ObjectMapper.Interfaces
{
    /// <summary>
    /// AutoMapper wrapper specification shared between all the Kyo projects.
    /// </summary>
    /// <typeparam name="TSource">Type of the source object</typeparam>
    /// <typeparam name="TDestination">Type of the destination object</typeparam>
    public interface ISharedMapper<TSource, TDestination> where TSource : class, new() where TDestination : class
    {
        /// <summary>
        /// Map the source object to the destination object.
        /// </summary>
        /// <param name="source">TSource object</param>
        /// <returns>TDestination object</returns>
        TDestination Map(TSource source);

        /// <summary>
        /// Map the destination object to the source object.
        /// </summary>
        /// <param name="destination">TDestination object</param>
        /// <returns>TSource object</returns>
        TSource Map(TDestination destination);

        /// <summary>
        /// Maps the source Expression to the destination Expression.
        /// </summary>
        /// <typeparam name="TSourceEx"></typeparam>
        /// <typeparam name="TDestinationEx"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        TDestinationEx Map<TSourceEx, TDestinationEx>(TSourceEx source);
    }
}
