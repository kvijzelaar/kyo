﻿using Autofac;
using System.Reflection;

namespace Kyo.Shared.DependencyInjection.Interfaces
{
    public interface IDependency
    {
        ContainerBuilder Register(ContainerBuilder builder, Assembly[] referencedAssemblies = null);
    }
}
