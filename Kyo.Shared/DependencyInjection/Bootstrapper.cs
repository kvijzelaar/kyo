﻿using System;
using Autofac;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Collections.Generic;
using Kyo.Shared.DependencyInjection.Interfaces;

namespace Kyo.Shared.DependencyInjection
{
	public static class Bootstrapper
	{
		public static ContainerBuilder Register(ContainerBuilder builder)
		{
			// Retrieve all the referenced assemblies.
			var referencedAssemblies = BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToArray();

			// Retrieve the dependency types.
			var dependencyTypes = GetAssemblyDependencyTypes(referencedAssemblies);

            if (dependencyTypes != null)
            {
                foreach (var dependencyType in dependencyTypes)
                {
                    // Create a instance of the dependency.
                    var dependency = Activator.CreateInstance(dependencyType) as IDependency;

                    // Register the dependency.
                    if (dependency != null)
                        builder = dependency.Register(builder, referencedAssemblies);
                }
            }

            return builder;
        }

		#region Helpers

		private static IEnumerable<Type> GetAssemblyDependencyTypes(Assembly[] referencedAssemblies)
		{
            if (referencedAssemblies != null)
            {
                var dependencyType = typeof(IDependency);

                return referencedAssemblies.SelectMany(e => e.GetTypes().Where(t => !t.IsInterface && dependencyType.IsAssignableFrom(t)));
            }

            return null;
		}

		#endregion
	}
}
